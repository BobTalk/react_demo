/*
 * @Author: your name
 * @Date: 2022-02-08 15:43:49
 * @LastEditTime: 2022-02-08 16:03:13
 * @LastEditors: your name
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: /react_demo/src/App.js
 */
import React from 'react';
import './App.css';
// import { SlidingWindowScrollHook } from "./SlidingWindowScrollHook";
import SlidingWindowScroll from "./SlidingWindowScroll";
import MY_ENDLESS_LIST from './Constants';
function App() {
  return (
    <div className="App">
      <h1>15个元素实现无限滚动</h1>
       <SlidingWindowScroll list={MY_ENDLESS_LIST} height={195}/>
      {/* <SlidingWindowScrollHook list={MY_ENDLESS_LIST} height={195}/> */}
    </div>
  );
}
export default App;
